; -------------------------------------------------------------------------
; Tables, please place them somewhere where is space
; Would fit after 2024 ($7E8) for example
; -------------------------------------------------------------------------

.export gamespr_bit,watercanspr_bit,rakespr_bit,sprite_addr,firemonster_colors,mirror_flag

gamespr_bit:     .byte 0, 1<<GAMESPR_NO
watercanspr_bit: .byte 0, 1<<WATERCANSPR_NO
rakespr_bit:     .byte 0, 1<<RAKESPR_NO

sprite_addr:     .byte <(64*(GAMESPRCOSTUME_RIGHT+1)),>(64*(GAMESPRCOSTUME_RIGHT+1)), <(64*WATERCANSPRCOSTUME),>(64*WATERCANSPRCOSTUME)
                 .byte <(64*RAKESPRCOSTUME),>(64*RAKESPRCOSTUME), <(64*FLASKSPRCOSTUME),>(64*FLASKSPRCOSTUME)

firemonster_colors: .byte 68,69,73 ;,0 ATTENTION, uses 0 from next table!!!

mirror_flag:	 .byte $00,$40

