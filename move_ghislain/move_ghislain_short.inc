; -------------------------------------------------------------------------
; Code to move the player sprite
; Depening on the status in the variable carrying,
; carried sprites are moved together with the player
; this routine has fixed speed of 2 pxl/call for the sprite
; the X position alsways needs to be an odd number
; -------------------------------------------------------------------------

	ldy #00
	sty moved
	iny	; instead of ldy #01
	sty flips ; allow maximum 1 flip

smx:    ldx #GAMESPRCOSTUME_DOWN	;costume worn by sprite

        lda $DC02-JOYPORT
checkup:
        lsr 
        bcs checkdown
      
        ldx #GAMESPRCOSTUME_UP
        ldy playery
        cpy #PLAYER_MIN_Y
        bcc checkdown
      
        dey
	dey
        sty playery
        inc moved
      
checkdown:
        lsr    ;down dir into carry
        bcs checkleft
      
        ldx #GAMESPRCOSTUME_DOWN
        ldy playery
        cpy #PLAYER_MAX_Y
        bcs checkleft
      
	iny
	iny
	sty playery
        inc moved
      
checkleft:
        lsr    ;left dir into carry
        bcs checkright
      
        ldx #GAMESPRCOSTUME_RIGHT
        ldy #LOOKLEFT
        sty dir

        ldy playerx_hi
        bne moveleft
	ldy playerx
        cpy #PLAYER_MIN_X
        bcc checkright
      
moveleft:
        dec playerx
	bne no_underflow
        dec playerx_hi
no_underflow:
	dec playerx
        inc moved
      
checkright:
        lsr   ;right dir into carry
        bcs checkfire
      
        ldx #GAMESPRCOSTUME_RIGHT
        ldy #LOOKRIGHT
	sty dir

        ldy playerx_hi
        beq moveright
	ldy playerx
        cpy #<PLAYER_MAX_X
        bcs checkfire
      
moveright:
	inc playerx
	bne no_overflow
	inc playerx_hi
no_overflow:
	inc playerx
        inc moved  
      
checkfire:
        lsr    ;fire bit into carry
        bcs afterfire
        inc firecounter
        lda #FIRETRIGGER
	cmp firecounter
	bne afterfire

	.include "get_scraddr_at_player.inc"
.if COMPILE_AS_FUNCTION=1
	jsr fire_action
.else
        .include "fire_action.inc"
.endif

afterfire:
; ----------------------------------------------
; pick up things
; ----------------------------------------------
	dec pickupcounter
	bpl endpickup
	inc pickupcounter	;set it back to 0
	lda $D01E	;Sprite-sprite collision register
	lsr
	bcc endpickup	;no collision with gamesprite
	and #3
	beq endpickup
	cmp #1
	bne case2
	lda #CARRYWATERCAN
	bne takeitem	;accu was loaded with nonzero value so this branch always jumps
case2:
	cmp #2
	bne case3
	lda #CARRYRAKE
	bne takeitem	;accu was loaded with nonzero value so this branch always jumps
case3:
	lda #PICKUPCOOLDOWN
	sta pickupcounter
	lda carrying
	eor #CARRYWATERCAN ^ CARRYRAKE	;switch between both


takeitem:
	sta carrying


endpickup:
	lda $D01E	;reset sprite-sprite collision register
; ----------------------------------------------
; placement
; ----------------------------------------------placement:
        stx smx+1
	lda moved
        beq skipanim
	sta firecounter
	inc animcounter
skipanim:
	lda animcounter
	and #ANIMATIONSPEED
        beq same_costume
	inx
same_costume:
	stx SCREEN_BASE+$3f8+GAMESPR_NO

	;place play sprite and carried object
	ldy carrying
	lda playerx
	sta $d000+2*GAMESPR_NO
	sta $d000,y
	lda playery
	sta $d001+2*GAMESPR_NO
	sta $d001,y

	;transfer playerx_hi to carried object
	ldx playerx_hi
	stx playerx_hi-2*GAMESPR_NO,y

	;let's determine the address of the carried sprite
	lda sprite_addr-2*GAMESPR_NO,y
	sta pointer1
	lda sprite_addr-2*GAMESPR_NO+1,y
	sta pointer1+1

	;calculate 9th bits
	;ldx playerx_hi   ;because X is already loaded with playerx_hi
	lda gamespr_bit,x
	clc
	ldx watercanx_hi
	adc watercanspr_bit,x
	ldx rakex_hi
	adc rakespr_bit,x
	sta $d010

; ----------------------------------------------
; flipping sprites
; ----------------------------------------------

	cli  ;flipping can take longer, so lets allow interrupts

        ldx dir ;direction our sprite watches
	jsr mirror_sprite ;flip the carried item

	;flip the left/right sprites
	lda #<(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1
	lda #>(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1+1
	ldx dir
	jsr mirror_sprite

	lda #<(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1
	lda #>(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1+1
	ldx dir
.if COMPILE_AS_FUNCTION=1
	jmp mirror_sprite
.else
	jsr mirror_sprite
.endif

