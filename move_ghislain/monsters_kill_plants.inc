; -------------------------------------------------------------------------
; Monsters kill plants
; A and X need to be preserved
; Monster type in X, Monster Y coordinate in A
; -------------------------------------------------------------------------

monsters_kill_plants:
	pha
	and #7
	bne end
	pla
	pha
	lsr
	lsr
	lsr
	sec
	sbc #7
	bmi end
	cmp #20  ;only do this for 21 lines, numbered from 0 to 21, starting at $450
	bcs end

@l2:	ldy	#6
@l:
	lda	(monsterkillptr),y
;	lda 	#FLOWERSTEM
;	sta	(monsterkillptr),y	

	and	#%11111000
	cmp	#FLOWERSTEM	; Flower character▀
	bne	@nokill
	lda	#EMPTYCHAR
	sta	(monsterkillptr),y	
@nokill:
	dey
	bpl	@l	

	lda	#40
	clc
	adc	monsterkillptr
	sta	monsterkillptr
	bcc	@nohi
	inc	monsterkillptr+1
@nohi:

end:
	pla
	rts

