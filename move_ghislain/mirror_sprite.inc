; -------------------------------------------------------------------------
; Code to mirror the sprite at position (pointer1)
; -------------------------------------------------------------------------
optimize_size=1 ;if this is 0 you get sligtly faster, but longer code

mirror_sprite:       ldy #$3f   ; reverse all the bytes
	lda (pointer1),y
	sta sm_sprmucoflag+1
	eor mirror_flag,x
	and #$40
	beq exit_mirror_routine    ;sprite has already correct orientation

	dec flips
	bmi exit_mirror_routine    ;abandon if maximum number of flips exceeded

	lda (pointer1),y
	eor #$40
	sta (pointer1),y

flip:   dey
loop1:  lda (pointer1),y
	beq skip_reverse	;if the byte is empty, this saves us around 80 cycles
; ----------------------------------------------
; reverse the byte
; ----------------------------------------------
        sta temp
sm_sprmucoflag:	lda #00
	bmi reverse_muco

        ldx #$07
loop2:  rol temp ;highest bit in temp into carry
        ror      ;carry into lowest bit
        dex
        bpl loop2
        bmi end_reverse

;in case of a multicolor sprite, it is a bit more complicated
reverse_muco:
	ldx #03
loop3:  rol temp 
        rol temp ;two bits forward
        ror      ;bit into accu
        ror temp ;one backward
        ror      ;bit into accu
        rol temp ;one forward
        dex
        bpl loop3

end_reverse:
        sta (pointer1),y
skip_reverse:
        dey
        bpl loop1
; ----------------------------------------------
;now switch every left and right byte
; ----------------------------------------------
.if optimize_size=1
	ldy #62
loop4:
	lda (pointer1),y
	tax
	dey
	dey
	lda (pointer1),y
	iny
	iny
	sta (pointer1),y
	dey
	dey
	txa
	sta (pointer1),y
	dey

	bpl loop4

.else
; the following code is longer, but faster
pointer2=sprmucoflag

	lda pointer1
	ora #3
	sta pointer2
	lda pointer1+1
	sta pointer2+1

	ldy #62
loop4:
	lda (pointer1),y
	tax
	lda (pointer2),y
	sta (pointer1),y
	txa
	sta (pointer2),y
	dey
        dey
	dey

	bpl loop4
.endif

exit_mirror_routine:
	rts
	

