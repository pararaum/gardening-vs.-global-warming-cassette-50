; -------------------------------------------------------------------------
; Code to set up the sprs (color, costume, initial position)
; -------------------------------------------------------------------------

	ldx #CARRYNOTHING
	stx carrying

        lda #PLAYERSTARTX/2
        sta playerx
        lda #PLAYERSTARTY
        sta playery
        
        lda #WATERCANSTARTX/2
        sta watercanx
        lda #WATERCANSTARTY
        sta $d001+2*WATERCANSPR_NO
        
        lda #RAKESTARTX/2
        sta rakex
        lda #RAKESTARTY
        sta $d001+2*RAKESPR_NO

        lda #FLASKSTARTX/2
        sta flaskx
        lda #FLASKSTARTY
        sta $d001+2*FLASKSPR_NO

	;enable sprites
	lda # (1<<GAMESPR_NO)+(1<<WATERCANSPR_NO)+(1<<RAKESPR_NO)+(1<<FLASKSPR_NO)+(1<<MONSTERSPR_NO)
	sta $d015

	;we don't tamper with spr priority and expansion, because we assume the user
	;loads the game after a clean reset