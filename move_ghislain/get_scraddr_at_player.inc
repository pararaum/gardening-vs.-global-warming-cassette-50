	;calculate character address under sprite
OFFSET_SCR = SCREEN_BASE - 2 
	lda playery
	sec
	sbc #42
	lsr
	lsr
	lsr
	sta scrn_line

	tay
	lda #>OFFSET_SCR
	sta char_ptr+1
	lda #<OFFSET_SCR
@loopy0:
	clc
@loopy:	dey
	bmi @exit
	adc #40
	sta char_ptr
	bcc @loopy
	inc char_ptr+1
	bcs @loopy0

@exit:
	lda playerx
	lsr
	lsr

	tay
	lda #$40
	cmp (char_ptr),y
	bne foundplant
	iny
        cmp (char_ptr),y
	bne foundplant
	tya
	clc
	adc #39
	tay	
	lda #$40
        cmp (char_ptr),y
	bne foundplant
	iny
foundplant:
	clc
	tya
	adc char_ptr
	sta char_ptr
	bcc @nohi
	inc char_ptr+1
@nohi:
