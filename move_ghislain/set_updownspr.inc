; -*- mode: asm -*-
; ----------------------------------------------
; set up/down sprites
; ----------------------------------------------

.if COMPILE_AS_FUNCTION>0
.export animate_feet_offset
.exportzp install_up_sprite,install_down_sprite,setsprite_up_down,feet_eor_data,end_feet_eor_data
.endif

target_sprite=$c0

up_down_offset=$f
up_down_length= end_look_up - look_up

look_up:   .byte $03, $ff, $00, $03, $ff, $00, $03, $ff, $00, $02, $fe
end_look_up:

look_down: .byte $02, $aa, $00, $02, $ee, $00, $02, $aa, $00, $02, $aa
end_look_down:

install_up_sprite=end_look_up - look_up - 1
install_down_sprite=end_look_down-look_up-1

setsprite_up_down:
	pha
	ldy #up_down_length-1
@loop:
	lda look_up,x
	sta GAMESPRCOSTUME_UPDOWN*$40 + up_down_offset,y
	dex
	dey
	bpl @loop
	pla
	ldx #GAMESPRCOSTUME_UPDOWN
	rts

animate_feet_offset=$2a


feet_eor_data:
.byte $04, $00, $40, $08, $00, $80, $00, $00, $00, $00, $00, $00, $00, $00, $00, $03, $8b, $00, $03, $cf
end_feet_eor_data: