; -*- mode: asm -*-
; -------------------------------------------------------------------------
; Preset values
; -------------------------------------------------------------------------

.importzp playerx,playerx_hi,playery,watercanx,control,rakex,firecounter,moved,animcounter,carrying,pointer1,flips,dir,temp,char_ptr,scrn_line,pickupcounter,Xbit9,flaskx,monsterx, monster_type
.importzp	heat_resistance,cold_resistance,level

COMPILE_AS_FUNCTION=0
DISTRIBUTOR=1

GAMESPRCOSTUME_UPDOWN= 50 
;GAMESPRCOSTUME_DOWN  = 50 ;set these to the proper address/$40 later 
;GAMESPRCOSTUME_UP    = 52
GAMESPRCOSTUME_RIGHT = 54 
WATERCANSPRCOSTUME   = 56
RAKESPRCOSTUME       = 57
DEADPLANTSPRCOSTUME  = 58
FLASKSPRCOSTUME      = 59

SCREEN_BASE=$0400

JOYPORT= 2 ;use numbers 1 or 2
WALKSPEED= 2
FIRETRIGGER= 3 ;number of frames until firebutton triggers an action
ANIMATIONSPEED=(1<<3) ;needs to be an exponent of 2
PICKUPCOOLDOWN=40

GAMESPR_NO    =0
WATERCANSPR_NO=1
RAKESPR_NO    =2
FLASKSPR_NO   =3
MONSTERSPR_NO   =7

CARRYNOTHING  = 2*GAMESPR_NO
CARRYWATERCAN = 2*WATERCANSPR_NO
CARRYRAKE     = 2*RAKESPR_NO
CARRYFLASK    = 2*FLASKSPR_NO

SNOWMONSTER_MASK = $1f
FIREMONSTER_MASK = $03

SNOWMONSTER_VALUE = 100
FIREMONSTER_VALUE = $80

GAMESPRCOLOR=10
WATERCANSPRCOLOR=3
RAKESPRCOLOR=15
FLASKSPRCOLOR=1

SPRMUCO1=13
SPRMUCO2=9

GAMESPR_IS_MUCO=1
WATERCANSPR_IS_MUCO=0
RAKESPR_IS_MUCO=1
FLASKSPR_IS_MUCO=0

LOOKRIGHT=0
LOOKLEFT=1

PLAYERSTARTX=  149
PLAYERSTARTY=  100
WATERCANSTARTX=300
WATERCANSTARTY=80
RAKESTARTX=    300
RAKESTARTY=    130
FLASKSTARTX=    300
FLASKSTARTY=    180

MONSTERENDY=	254

PLAYER_MIN_Y=  58
PLAYER_MAX_Y=  228
PLAYER_MIN_X=  24
PLAYER_MAX_X=  324