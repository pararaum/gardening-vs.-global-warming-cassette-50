; -------------------------------------------------------------------------
; Code to invoke snow/fire monster
; pass the X position in register A
; and the monster type (SNOWMONSTER_VALUE,FIREMONSTER_VALUE) in register Y
; -------------------------------------------------------------------------

invoke_monster:
	sta monsterx
	sty monster_type
	lda #1
	sta $D027+MONSTERSPR_NO
	sta $D001+2*MONSTERSPR_NO