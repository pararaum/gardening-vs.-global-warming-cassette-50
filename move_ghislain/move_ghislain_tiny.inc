; -*- mode: asm -*-
; -------------------------------------------------------------------------
; Code to move the player sprite
; Depening on the status in the variable carrying,
; carried sprites are moved together with the player
; this routine has fixed speed of 2 pxl/call for the sprite
; the X position is stored in two pixel steps, so the sprite moves between X=12 and X=160
; -------------------------------------------------------------------------

.if COMPILE_AS_FUNCTION>0
.importzp animate_feet_offset
.importzp install_up_sprite,install_down_sprite,setsprite_up_down,feet_eor_data,end_feet_eor_data
.endif

animate_feet_length:=end_feet_eor_data-feet_eor_data

move_player:
	ldy #01
	sty flips ; allow maximum 1 flip

smx:    ldx #GAMESPRCOSTUME_UPDOWN	;costume worn by sprite

        lda $DC02-JOYPORT
checkup:
        lsr 
        bcs checkdown
      
        ldx #install_up_sprite
	jsr setsprite_up_down

        ldy playery
        cpy #PLAYER_MIN_Y
        bcc checkdown
      
        dey
	dey
        sty playery
        cli	;clear IRQ flag as flag that sprite was moved
      
checkdown:
        lsr    ;down dir into carry
        bcs checkleft
      
        ldx #install_down_sprite
	jsr setsprite_up_down

        ldy playery
        cpy #PLAYER_MAX_Y
        bcs checkleft
      
	iny
	iny
	sty playery
        cli	;clear IRQ flag as flag that sprite was moved
      
checkleft:
        lsr    ;left dir into carry
        bcs checkright
      
        ldx #GAMESPRCOSTUME_RIGHT
        ldy #LOOKLEFT
	sty dir
        ldy playerx
        cpy #PLAYER_MIN_X/2
        bcc checkright

        dec playerx
        cli	;clear IRQ flag as flag that sprite was moved

checkright:
        lsr   ;right dir into carry
        bcs checkfire
      
        ldx #GAMESPRCOSTUME_RIGHT
        ldy #LOOKRIGHT
	sty dir
        ldy playerx
        cpy #PLAYER_MAX_X/2
        bcs checkfire

	inc playerx
        cli	;clear IRQ flag as flag that sprite was moved
      
checkfire:
	php	;save status register with decimal flag for later

        lsr    ;fire bit into carry
        bcs afterfire
        inc firecounter
        lda #FIRETRIGGER	
	cmp firecounter
	bne afterfire

	.include "get_scraddr_at_player.inc"
.if COMPILE_AS_FUNCTION=1
	jsr fire_action
.else
        .include "fire_action.inc"
.endif

afterfire:
; ----------------------------------------------
; pick up things
; ----------------------------------------------
	dec pickupcounter
	bpl endpickup
	inc pickupcounter	;set it back to 0
	ldy carrying
	lda $D01E	;Sprite-sprite collision register
	bmi endpickup	;avoid pickup when monster touches any sprite
	lsr
	bcc endpickup	;no collision with gamesprite
	lsr
	bcc case2
	cpy #CARRYWATERCAN
	beq case2
	ldy #CARRYWATERCAN
	bne takeitem    ;unconditional jump because CARRYWATERCAN is always > 0

case2:  lsr
	bcc case3
	cpy #CARRYRAKE
	beq case3
	ldy #CARRYRAKE
	bne takeitem    ;unconditional jump because CARRYWATERCAN is always > 0
	
case3:  lsr
	bcc endpickup
	cpy #CARRYFLASK
	beq endpickup	
	ldy #CARRYFLASK

takeitem:
	lda #PICKUPCOOLDOWN
	sta pickupcounter
.if DISTRIBUTOR>0
	sty Xbit9
.else
	sty carrying
.endif

endpickup:
	;lda $D01E	;reset sprite-sprite collision register
; ----------------------------------------------
; animation
; ----------------------------------------------placement:
        stx smx+1
	pla
	and #04	;was interrupt flag set before aka did we move?
        bne skipanim
	sta firecounter
	inc animcounter
skipanim:
	lda animcounter

	cpx #GAMESPRCOSTUME_UPDOWN
	bne lr_animation
;animate feed

	and #7
	bne same_costume
	inc animcounter
animate_feet:
	ldy #animate_feet_length-1
@loop:	lda GAMESPRCOSTUME_UPDOWN*$40 + animate_feet_offset,y
	eor feet_eor_data,y
	sta GAMESPRCOSTUME_UPDOWN*$40 + animate_feet_offset,y
	dey
	bpl @loop
	bmi same_costume
lr_animation:
	and #ANIMATIONSPEED
        beq same_costume
next_costume:
	inx
same_costume:
	stx SCREEN_BASE+$3f8+GAMESPR_NO

; ----------------------------------------------
; place play sprite and carried object
; ----------------------------------------------
placement:
.if DISTRIBUTOR>0
	lda playery
	sta $d001+2*GAMESPR_NO  ;set y
	lda playerx
	asl
	sta $d000+2*GAMESPR_NO  ;set x
	ror
	sta $D010
.else
	lda $D01E	;reset sprite-sprite collision register

	ldy carrying
	lda playery
	sta $d001+2*GAMESPR_NO  ;set y
	sta $d001+2*GAMESPR_NO,y  ;set y of carried object 
	lda playerx
	sta playerx,y  ;in case we carry something, copy it there
	asl   ;x=x*2, bit 9 into carry
	sta $d000+2*GAMESPR_NO  ;set x
	ror Xbit9

	lda watercanx
	asl
	sta $d000+2*WATERCANSPR_NO  ;set x
	ror Xbit9
	
	lda rakex
	asl
	sta $d000+2*RAKESPR_NO  ;set x
	ror Xbit9

	lda flaskx
	asl
	sta $d000+2*FLASKSPR_NO  ;set x
	ror Xbit9
	lsr Xbit9
	lsr Xbit9
	lsr Xbit9
	
	lda monsterx
	asl
	sta $d000+2*MONSTERSPR_NO  ;set x
	lda Xbit9
	ror 
	sta $D010

	;let's determine the address of the carried sprite
	lda sprite_addr-2*GAMESPR_NO,y
	sta pointer1
	lda sprite_addr-2*GAMESPR_NO+1,y
	sta pointer1+1
.endif

; ----------------------------------------------
; flipping sprites
; ----------------------------------------------

	cli  ;flipping can take longer, so lets allow interrupts
        ldx dir ;direction our sprite watches
	jsr mirror_sprite ;flip the carried item

	;flip the left/right sprites
	lda #<(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1
	lda #>(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1+1
	ldx dir
	jsr mirror_sprite

	lda #<(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1
	lda #>(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1+1
	ldx dir

	jmp mirror_sprite


