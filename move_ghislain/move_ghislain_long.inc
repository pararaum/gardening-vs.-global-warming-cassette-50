; -------------------------------------------------------------------------
; Code to move the player sprite
; Depening on the status in the variable carrying,
; carried sprites are moved together with the player
; -------------------------------------------------------------------------

	ldy #00
	sty moved
	iny	; instead of ldy #01
	sty flips ; allow maximum 1 flip

smy:    ldy #GAMESPRCOSTUME_DOWN	;costume worn by sprite

        lda $DC02-JOYPORT
	sta control
checkup:
        lsr control   ;up dir into carry
        bcs checkdown
      
        ldy #GAMESPRCOSTUME_UP
        lda playery
        cmp #PLAYER_MIN_Y
        bcc checkdown
      
        sbc #WALKSPEED ;carry is known to be set here
        sta playery
        inc moved
        stx firecounter
      
checkdown:
        lsr control   ;down dir into carry
        bcs checkleft
      
        ldy #GAMESPRCOSTUME_DOWN
        lda playery
        cmp #PLAYER_MAX_Y
        bcs checkleft
      
        adc #WALKSPEED ;carry is known to be cleared here
        sta playery
        inc moved
        stx firecounter
      
checkleft:
        lsr control   ;fire bit into carry
        bcs checkright
      
        ldy #GAMESPRCOSTUME_RIGHT
        ldx #LOOKLEFT
        stx dir
        lda playerx
        ldx playerx_hi
        bne moveleft
        cmp #PLAYER_MIN_X
        bcc checkright
      
moveleft:
        sec
        sbc #WALKSPEED
        sta playerx
        txa
        sbc #00
        sta playerx_hi
        inc moved
        stx firecounter
      
checkright:
        lsr control   ;right dir into carry
        bcs checkfire
      
        ldy #GAMESPRCOSTUME_RIGHT
        ldx #LOOKRIGHT
	stx dir
        lda playerx
        ldx playerx_hi
        beq moveright
        cmp #<PLAYER_MAX_X
        bcs checkfire
      
moveright:
        ;clc  ;carry is known to be cleared here
        adc #WALKSPEED
        sta playerx
        txa
        adc #00
        sta playerx_hi
        inc moved  
        stx firecounter
      
checkfire:
        lsr control   ;right dir into carry
        bcs placement
        inc firecounter
        lda #FIRETRIGGER
	cmp firecounter
	bne placement
.if COMPILE_AS_FUNCTION=1
	jsr fire_action
.else
        .include "fire_action.inc"
.endif

placement:
        sty smy+1
	lda moved
        beq skipanim
	inc animcounter
skipanim:
	lda animcounter
	and #ANIMATIONSPEED
        beq same_costume
	iny
same_costume:
	sty SCREEN_BASE+$3f8+GAMESPR_NO

	;place play sprite and carried object
	ldy carrying
	lda playerx
	sta $d000+2*GAMESPR_NO
	sta $d000,y
	lda playery
	sta $d001+2*GAMESPR_NO
	sta $d001,y

	;transfer playerx_hi to carried object
	lda playerx_hi
	sta playerx_hi-2*GAMESPR_NO,y

	;let's determine the address of the carried sprite
	lda sprite_addr-2*GAMESPR_NO,y
	sta pointer1
	lda sprite_addr-2*GAMESPR_NO+1,y
	sta pointer1+1

	;calculate 9th bits
	ldy playerx_hi
	lda gamespr_bit,y
	clc
	ldy watercanx_hi
	adc watercanspr_bit,y
	ldy rakex_hi
	adc rakespr_bit,y
	sta $d010

; ----------------------------------------------
; flipping sprites
; ----------------------------------------------

	cli  ;flipping can take longer, so lets allow interrupts

        ldx dir ;direction our sprite watches
	jsr mirror_sprite ;flip the carried item

	;flip the left/right sprites
	lda #<(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1
	lda #>(64*GAMESPRCOSTUME_RIGHT)
	sta pointer1+1
	ldx dir
	jsr mirror_sprite

	lda #<(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1
	lda #>(64*(GAMESPRCOSTUME_RIGHT+1))
	sta pointer1+1
	ldx dir
.if COMPILE_AS_FUNCTION=1
	jmp mirror_sprite
.else
	jsr mirror_sprite
.endif

