; -------------------------------------------------------------------------
;This is the main file for doing a unit test with the Ghislain sprites
;Code in this file is just auxiliary and is not intended for the final game
;By including the other code parts this file can be assembled into a
;stand-alone program
; -------------------------------------------------------------------------

.include "presets_unit_test.inc"


;stuff only relevant for unit test

RASTERLINE=240

program_start:

; -----------------------------------------
;call the setup function for the sprites
; -----------------------------------------

	.include "init_once_sprites.inc"
	.include "set_sprite_starting_positions.inc"

; -----------------------------------------
;let's set up a RASTERLINE IRQ 
; -----------------------------------------
	sei
	;set irq sources
	lda #$7f
	and $D011	; accu is still $7f
	sta $D011 	; Clear most significant bit in VIC's raster register

	sta $dc0d	; disable timer interrupts
	sta $dd0d
	lda $dc0d	; acknowledge CIA interrupts

	lda #<RASTERLINE		; Set the raster line number where interrupt should occur 
	sta $D012 	
	lda $D011
	.if >RASTERLINE = 0
	  and #$7f
	.else
	  ora #$80
	.endif
	sta $D011

	lda #01		; set raster interrupt
	sta $D01A 	

	;set ISR addr
	lda #<isr0
	sta $314
	lda #>isr0
	sta $315
	cli

	rts

;-----------------------------------------------
;IRQ routine for line 0
;-----------------------------------------------
isr0:	asl $d019
	.include "move_monsters.inc"	

	jsr move_player
	jmp $ea31
;-----------------------------------------------
;Moving subroutine
;-----------------------------------------------

	.include "move_ghislain_tiny.inc"

	.include "set_updownspr.inc"

;-----------------------------------------------
;Monsters kill plants
;-----------------------------------------------

heat_resistance:	.res 1
cold_resistance:	.res 1
level:	.res 1

;	.include "monsters_kill_plants.inc"	
monsters_kill_plants:  ;in the unit test they don't
	rts

;-----------------------------------------------
;Mirroring subroutine
;-----------------------------------------------
	.include "mirror_sprite.inc"

;-----------------------------------------------
;Tables
;-----------------------------------------------
	.include "tables.inc"
