;;; -*- mode: asm  -*-
; -------------------------------------------------------------------------
; Code to move snow/fire monster
; -------------------------------------------------------------------------

move_monsters:
	lda $D001+2*MONSTERSPR_NO
	beq end_move_monsters
	inc $D001+2*MONSTERSPR_NO
	ldx monster_type
	bmi animate_firemonster

animate_snowmonster:
	ldy cold_resistance
	cpy level
	bcs justsnowshow     ; Enough resistance

	jsr monsters_kill_plants

justsnowshow:
	and #SNOWMONSTER_MASK
	bne end_move_monsters
	txa
	eor #100 ^ 101    ;switch between 100 and 101
	tax
	sta SCREEN_BASE+$3f8+MONSTERSPR_NO	
	bne save_monster_type

animate_firemonster:
	ldy heat_resistance
	cpy level
	bcs justfireshow     ; Enough resistance

	jsr monsters_kill_plants

justfireshow:
	and #FIREMONSTER_MASK
	bne end_move_monsters
	inx
	cpx #$83
	bne @no_x_wrap
	ldx #$80
@no_x_wrap:

	lda firemonster_colors-$80,x
	sta SCREEN_BASE+$3f8+MONSTERSPR_NO
	and #1
	clc
	adc #7
	sta $D027+MONSTERSPR_NO
save_monster_type:
	stx monster_type
	bne end_move_monsters

end_move_monsters:
