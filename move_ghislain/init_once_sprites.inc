; -------------------------------------------------------------------------
; Code to set up the sprs (color, costume, initial position)
; -------------------------------------------------------------------------


	;colors
	lda #GAMESPRCOLOR
	sta $d027 + GAMESPR_NO
	lda #WATERCANSPRCOLOR
	sta $d027 + WATERCANSPR_NO
	lda #RAKESPRCOLOR
	sta $d027 + RAKESPR_NO
	lda #FLASKSPRCOLOR
	sta $d027 + FLASKSPR_NO
	lda #SPRMUCO1
	sta $d025
	lda #SPRMUCO2
	sta $d026

	;set spr costumes
	lda #WATERCANSPRCOSTUME
	sta SCREEN_BASE+$3f8+WATERCANSPR_NO
	lda #RAKESPRCOSTUME
	sta SCREEN_BASE+$3f8+RAKESPR_NO
	lda #FLASKSPRCOSTUME
	sta SCREEN_BASE+$3f8+FLASKSPR_NO

	;set sprite multicolors
	lda # (GAMESPR_IS_MUCO<<GAMESPR_NO)+(WATERCANSPR_IS_MUCO<<WATERCANSPR_NO)+(RAKESPR_IS_MUCO<<RAKESPR_NO)+(FLASKSPR_IS_MUCO<<FLASKSPR_NO)
	sta $d01c