; -*- mode: asm -*-
; -------------------------------------------------------------------------
;Code to be executed when firebutton was pressed long enough
; -------------------------------------------------------------------------


	ldy #0
	lda (char_ptr),y
	clc
	adc #1
	sta (char_ptr),y

	
	;monster invocation
	ldy #SNOWMONSTER_VALUE
	lda playerx
	pha
	and #2
	bne @firemonster
	ldy #FIREMONSTER_VALUE
@firemonster:
	pla

	.include "invoke_monsters.inc" 

	