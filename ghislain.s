;;; Functions related to the movement of the Ghislain sprite.

	.include	"vars.i"
	.include	"consts.i"
	.include	"presets.i"
	.include	"muzak.i"
	.macpack	generic

	.export	fire_action
	.export	set_sprite_starting_positions
	.export sprite_movement
	.export	analyse_dead_plant

	.import monsters_kill_plants

;kill_weed, water_flower and analyse_dead_plant are assumed to be on same page
;if this is not the case, assembly will stop with a failed asertion
;enabling this saves 9 bytes in code and 2 bytes in ZP
.define ASSUME_SAME_PAGE 0


;;; Action called via interrupt when the fire button is pressed.
;;; Input: char_ptr
;;; Modifies: A, Y (X will be restored)

.proc	fire_action

	;; 	.segment	"SIXTEEN"
	.zeropage
.if ASSUME_SAME_PAGE=0
fptr:	.word	0		; Function pointer for handle plant
.endif
char:	.byte	0		; Character to compare to in handle plant
	.code
	txa
	pha

	lda	carrying
	cmp	#CARRYRAKE
	bne	@norake
.if ASSUME_SAME_PAGE=0
	lda	#<kill_weed
	sta	fptr
	lda	#>kill_weed
	sta	fptr+1
.else
	lda	#<kill_weed
	sta	sm_fptr+1   ;selfmodification of jump function
.endif
	lda	#WEEDCHAR
	sta	char
	jsr	handle_plant
@norake:
	cmp	#CARRYWATERCAN
	bne	@nocan
.if ASSUME_SAME_PAGE=0
	lda	#<water_flower
	sta	fptr
	lda	#>water_flower
	sta	fptr+1
.else
	lda	#<water_flower
	sta	sm_fptr+1   ;selfmodification of jump function
.endif
	lda	#FLOWERCHAR
	sta	char
	jsr	handle_plant
	jsr	play_watering
@nocan:
	cmp	#CARRYFLASK
	bne	@noflask
.if ASSUME_SAME_PAGE=0
	lda	#<analyse_dead_plant
	sta	fptr
	lda	#>analyse_dead_plant
	sta	fptr+1
.else
	lda	#<analyse_dead_plant
	sta	sm_fptr+1   ;selfmodification of jump function
.endif
	lda	#FLOWERCHAR
	sta	char
	jsr	handle_plant
@noflask:
	pla
	tax
	rts
;;; fptr is called if char_ptr points to the *head*.
handle_plant:
	ldy	#0
	jsr	check
	bne	go_up
.if ASSUME_SAME_PAGE=0
jmp_fp:	jmp	(fptr)
.else
sm_fptr:	;label for self-modifying poke
jmp_fp:	jmp	kill_weed  ;"kill_weed" is only here as a placeholder and for getting the high byte right
.endif
go_up:	lda	char_ptr
	pha
	sub	#40
	sta	char_ptr
	lda	char_ptr+1
	pha
	sbc	#0
	sta	char_ptr+1
	jsr	check
	bne	@noplant
	jsr	jmp_fp		; Trick for an indirekt JSR.
@noplant:
	pla			; Restore old pointer.
	sta	char_ptr+1
	pla
	sta	char_ptr
	rts
check:
	lda	(char_ptr),y	; Get the value of the character.
	;; Check if current character is a plant character (STEM!).
	and	#%11111100	; Mask out different weeds.
	cmp	char		; Do the comparison...
	rts			; And return result in Z.
;;; This function always kills the head and the stem which is below. Make sure that char_ptr points to the right cell.
kill_weed:
	lda	#EMPTYCHAR
	sta	(char_ptr),y
	ldy	#40
	sta	(char_ptr),y
	lda	#WEEDPOINTS
	jmp	increase_score
water_flower:
	lda	char_ptr+1	; Save high-byte.
	pha
	add	#$d8->SCREEN_BASE
	sta	char_ptr+1
	lda	(char_ptr),y	; Get colour value.
	and     #$0f
	cmp	#DEADFLOWERCOLOUR
	beq	@no
	cmp	#WATEREDFLOWERCOLOUR
	beq	@no
	lda	#WATEREDFLOWERCOLOUR
	sta	(char_ptr),y	; Turn flower blue.
	pla
	sta	char_ptr+1	; Restore pointer.
	lda	(char_ptr),y	; Re-get value of character.
	and	#%00000011	; Value is now 0..3.
	asl			; Multiply by two
	adc	#1		; Carry is zero: 1, 3, 5, 7
	jmp	increase_score
@no:
	pla
	sta	char_ptr+1	; Restore pointer.
	rts
.endproc


.proc	analyse_dead_plant
	jsr	fire_action::kill_weed	; Actually just remove the chars and increase score.
	lda	$d027+FLASKSPR_NO ; Get sprite colour
	and	#$0f		  ; Mask colour bits.
	cmp	#HEATFLASKCOL	  ; Is it heat?
	beq	heatinc
	inc	cold_resistance
	rts
heatinc:
	inc	heat_resistance
	rts
.endproc

.if ASSUME_SAME_PAGE>0
.assert   >fire_action = >analyse_dead_plant, error, "water_flower and kill_weed might not on the same page, reorder functions in code"
.endif

; -----------------------------------------
;call the setup function for the sprites
; -----------------------------------------
;;; Modifies: A, X
;;; ARGL! Can not use .proc here as something with scoping makes trouble.
;;; The assembler complains that CARRYNOTHING is not a constant expression, setting CARRYNOTHING = ::CARRYNOTHING solves the problem. Too troublesome therefore we just use the old-style functions.
set_sprite_starting_positions:
	.include "move_ghislain/set_sprite_starting_positions.inc"
	rts

	;; mirror_sprite:
	.include "move_ghislain/mirror_sprite.inc"

sprite_movement:
	.include "move_ghislain/move_monsters.inc"	

	.include "move_ghislain/move_ghislain_tiny.inc"

	;tables are now at end of VECTOR segment
	.import gamespr_bit,watercanspr_bit,rakespr_bit,sprite_addr,firemonster_colors,mirror_flag
	;.include "move_ghislain/tables.inc"

;;; ❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀
	.end
;;; ❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀❀
