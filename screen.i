;;; -*- mode: asm -*-

;;; Clear(fill) the colourram.
;;; Input: - ⁖ for fill: A=colour to fill
;;; Output: X=0
;;; Modifies: A, X
	.import clear_colourram
	.import fill_colourram

	.import	clear_screen
	.import	fill_screen
