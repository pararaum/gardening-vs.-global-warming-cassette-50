;;; -*- mode: asm -*-
;	Ghislain sprite related functions.

; -----------------------------------------
;call the setup function for the sprites
; -----------------------------------------
;;; Input: -
;;; Output: -
;;; Modifies: A, X
				;
;;; ARGL! Can not use .proc here as something with scoping makes trouble.
;;; The assembler complains that CARRYNOTHING is not a constant expression, setting CARRYNOTHING = ::CARRYNOTHING solves the problem. Too troublesome therefore we just use the old-style functions.
	.import	set_sprite_starting_positions


; -------------------------------------------------------------------------
; Code to move the player sprite
; Depening on the status in the variable carrying,
; carried sprites are moved together with the player
; -------------------------------------------------------------------------
;;; Input: -
;;; Output: -
;;; Modifies: A, X, Y
	.import sprite_movement
