#! /usr/bin/make -f

AS=ca65
LD=ld65
CC=cl65
CFLAGS=-C cc65.cfg
LDFLAGS=-C cc65.cfg -L../c64playground/library
ASFLAGS=-I../c64playground/library -I../c64playground/includeCC65

TARGET=gavsgw
OBJS=gavsgw.o screen.o monster.o plants.o world.o header.o ghislain.o consts.o vars.o destructibles.o muzak.o chars.o sprites.o move_ghislain/zp_variables.o

ifdef DEBUG
ASFLAGS += -DDEBUG
endif

.PHONY:	all bin
all:	bin

clean:
	rm -f $(TARGET) *.o *.prg *.map *.labels

bin:	$(TARGET)

$(TARGET).prg:	$(TARGET)
	./xipz -a qadz -j 0x32a	$+

gavsgw:	$(OBJS)
	$(LD) $(LDFLAGS) -m $@.map -Ln $@.labels -o $@ $+ libT7D.lib
	@sed '/^Segment list/,/^$$/p' -n gavsgw.map

#$(LD) $+
