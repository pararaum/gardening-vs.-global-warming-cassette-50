;;; -*- mode: asm -*-

;;; Setup the SID, just clearing the registers.
;;; DANGER! This code is in ONCE. Call it early.
;;; Input: -
;;; Output: -
;;; Modifies: A, X
	.import	setupsid

	.import stop_sfx
	.import	play_bing

;;; Play the sawtooth sound.
;;; Input: -
;;; Output: -
;;; Modifies: A, X
	.import	play_saw

;;; Play the hissing sound when the monster comes.
;;; Input: -
;;; Output: -
;;; Modifies: A
	.import	play_hiss

	.import	play_watering
