;;; Important global variables.
	.include	"muzak.i"
	.exportzp	level
	.exportzp	score
	.exportzp	time
	.exportzp	game_running
	.exportzp	ptr
	.exportzp	ptr2
	.exportzp	tmp
	.exportzp	tmp2
	.export	increase_score
	.exportzp	randomoff
	.exportzp	heat_resistance
	.exportzp	cold_resistance
	.exportzp	updating_world

	.zeropage
;;; This is the current game level.
level:	.res	1

;;; Score as a 16 bit number.
score:	.res	2

	.zeropage
;;; Time as a 16 bit number.
time:	.res	2

game_running:
	.byte	0
randomoff:
	.res	1	; Pointer for new PRNG seeds.

	.zeropage
ptr:	.res	2
ptr2:	.res	2

tmp:	.res	1
tmp2:	.res	1
updating_world: .res 1    ;flag indicating if world is currently updating

	.zeropage
heat_resistance:	.res	1
cold_resistance:	.res	1

	.code
.proc	increase_score
	clc
	adc	score
	sta	score
	bcc	out
	inc	score+1
out:
	jmp	play_bing
.endproc
