;;; -*- mode: asm -*-
;;; The sprite segment actually begins at $340 (/ #x340 64) 13th sprite as in $300 to $33f KERNAL vectors are located which we need...
;;; Sprite data and useful functions.


;;;  * (/ #x40 64)	1	Ghislain moving
;;;  *			2	Ghislain moving
;;;  * (/ #xc0 64)	3	Watering can
;;;  * (/ #x140 64)	5	Ghislain moving
;;;  *			6	Ghislain moving
;;;  * (/ #x1c0 64)	7	Rake
;;;  * (/ #x340 64)	13	Ghislain moving right/left
;	.import	spr_right0
;;;  * (/ #x380 64) 	14	Ghislain moving right/left
;	.import spr_right1
;;;  * (/ #x3c0 64)	15	Dead plant
;	.import spr_deadplant
;;;  * (/ #x780 64)	30	Erlenmeyer Flask

