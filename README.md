# Gardening vs Global Warming #

In 2050 it became finally clear that humanity failed to stop global warming. While scientists discovered that several tipping points had been reached and an irreversible damage had been done the ecosystem, economists took the opportunity and pointed out that it makes no sense to burn further money on the reduction of carbon dioxide as humans are going to burn anyway. The slogan "Earn till you Burn!" was cast and successfully promoted further economic growth instead of environmental conservatism.

Quickly "ecological activism" was recast to "ecological terrorism" and movements like "Fridays for Future" outlawed, head figures of these movements publicly executed. The рубль rolled again...

In this dying world you are Ghislain, a French gardener with his share of prison experience for crimes like "Tree Hugging", "Plant restoration", etc. In your last solitary confinement you realised that you have to fight the system from within. Now you are producing and selling flowers and the neoliberal forces are happy. You are happy, too, as you can work with plants. You know that it is a fight you can not win but every bit helps and your tasks is, should you chose to accept it... Protect and grow flowers!

For downloads and a browser playable version go to https://pararaum.itch.io/gardening-vs-global-warming.
