;;; Characters for the game, according to the memory map the font will
;	be at $200, the first character at $200 '@' is being cleared
;	after initialisation, see destructibles.s.
	.include	"consts.i"
	.include	"monster.i"
	.include	"presets.i"
	.include	"muzak.i"
	.include	"vars.i"

	.export	handle_monster
	
	.segment	"CHARS"
	;; Character data. Starts at $208 (screen code $41)
	.byte                                 $00,$6e,$6e,$00,$00,$76,$76,$00
charcode:
.proc handle_monster
	lda	$D001+2*MONSTERSPR_NO ; Get Y: 0=monster movement has ended
	bne	end
	dec	monstercounter
	bne	end
	lda	monsterval		; Heat/Cold/Heat/Colt/…
	eor	#SNOWMONSTER_VALUE^FIREMONSTER_VALUE
	sta	monsterval	; Store current monster
	sta	monster_type	; This is used for the monster animation.
	lda	#255
	sbc	level		; Subtract level (no setting carry as we ran out of space).
	sta	monstercounter
	jsr	create_monster
	jmp	play_hiss
;end:	rts	see end of next function, we nned every byte here!
.endproc

.export wait_x_frames  ;waits as many frames (1/50 s) as the value in X reg
.proc wait_x_frames

@l1:	bit	$d011
	bmi	@l1
@l2:	bit	$d011
	bpl	@l2
	dex
	bne	@l1
	stx	$c6	;put a 0 there, because VICE sometimer injects keypresses at startup
	stx	$278	;put a 0 there, because VICE sometimer injects keypresses at startup
.endproc
end:	rts

charcode_end:
	.res	$30-(charcode_end-charcode)
	.out	.sprintf("▦Code bytes left in CHARS: $%02X", 30-(charcode_end-charcode))
	.byte $38,$10,$d4,$78,$34,$5c,$38,$10,$38,$10,$d4,$78,$34,$5c,$38,$10
	.byte $38,$10,$d4,$78,$34,$5c,$38,$10,$08,$08,$18,$50,$74,$3c,$18,$10
	.byte $00,$00,$6c,$dc,$ba,$ba,$e6,$7c,$00,$00,$6c,$74,$fa,$fe,$fe,$7c
	.byte $08,$18,$ff,$6c,$34,$6e,$ff,$18,$00,$38,$6c,$de,$be,$fe,$fe,$74
	.byte $38,$28,$38,$10,$54,$7c,$38,$10,$36,$1c,$48,$38,$1c,$3a,$48,$08
	.byte $30,$78,$1c,$36,$70,$d8,$30,$10,$10,$30,$78,$dc,$16,$30,$58,$10
	.byte $00,$00,$00,$00,$00,$00,$00,$10,$00,$00,$00,$00,$00,$40,$e0,$b2
	.byte $00,$38,$54,$aa,$d6,$aa,$54,$38,$00,$ba,$54,$ba,$aa,$ba,$54,$ba
