	.include	"consts.i"
	.include	"vars.i"
	.include	"prng.xorshift.i"
	.include	"plants.i"
	.include	"monster.i"
	.import	handle_monster	; Code memory problems...
	.import wait_x_frames 
	.macpack	generic

	.export	update_world
	.export	ripen_weed

	TOPLINE = 2			; Beginning line of the game world.
	TOPADDR = SCREEN_BASE+TOPLINE*40
	ENDADDR = SCREEN_BASE+__SCREEN_SIZE__
	NTHFRAME = 4*50+9		; Nearly every 4s

	.zeropage
irqptr:	.res	2		; If you change this, *restore* it!
	.zeropage
;;; Every nth frame something is done with the monster.
frame_stepper:	.byte	0	;NTHFRAME
;;; Flags storing different stati.
;;; bit 7: either ripen or explode, 0 = ripen, 1 = explode
frame_flags:	.byte	0

	.if 0
	.export	divide40
;;; Divide by 40 in order to get the screen line. Value must be below 1024!
;;; This routine is $1f bytes long.
;;; Input: A/X (LO/HI)
.proc	divide40
	.zeropage
dtmp:	.res	1
	.code
	sta	dtmp
	txa
	;; Divide by eight.
	lsr
	ror	dtmp
	lsr
	ror	dtmp
	lsr
	ror	dtmp
	;; Based on https://www.codebase64.org/doku.php?id=base:8bit_divide_by_constant_8bit_result
	;; Divide by 5: 18 bytes, 30 cycles
	lda	dtmp		; First of all get the lobyte into A.
	lsr
	adc	#13
	adc	dtmp
	ror
	lsr
	lsr
	adc	dtmp
	ror
	adc	dtmp
	ror
	lsr
	lsr
	rts
.endproc
	.endif

	.code
.proc	dry_flower
	sta     rcv_char_type+1
	and	#%11111100	; Mask flower bits out.
	cmp	#FLOWERCHAR
	bne	out
	jsr	xorshift	; Get next random number.
	and	#%01100010	; Only in … of the cases.
	bne	out		; If any of the bits is set the result is non-zero and we skip.
	lda	irqptr+1	; High byte of screen pointer.
	pha			; Save it.
	clc
	adc	#$d8->SCREEN_BASE ; Now pointing to the screen colour ram.
	sta	irqptr+1
	lda	(irqptr),y	     ; Get value from colour RAM.
	and	#$0F		     ; Use only lower nibble.
	cmp	#WATEREDFLOWERCOLOUR ; Watered flower is immune
	bne	checkdead
.FEATURE addrsize
.if .ADDRSIZE(flowercolours) = 1
  ;zeropage, use a complicated overflow fomrula
  flowercolours_offset_address = <($100 + flowercolours - FLOWERCHAR) 
.else
  ;not in zeropage
  flowercolours_offset_address = flowercolours - FLOWERCHAR
.endif
rcv_char_type:
	ldx	#00   ;value will be overwritten
	lda	flowercolours_offset_address,x	;restore original color
	bpl	setcolor	;unconditional branch since there is no color over $80
checkdead:
	cmp	#DEADFLOWERCOLOUR
	beq	dead
	cmp	#DRIEDFLOWERCOLOUR ; Compare to brown
	bne	fresh
	lda	#DEADFLOWERCOLOUR ; Dark gray, now dead.
	sta	(irqptr),y
	ldy	#40
	sta	(irqptr),y
	bne	restore_irqptr	; 11 != 0
fresh:
	lda	#DRIEDFLOWERCOLOUR
setcolor:
	sta	(irqptr),y	; Plant does not feel well.
	;; 	ldy	#40
	;; 	sta	(irqptr),y
restore_irqptr:
dead:
	pla			; Get old high-byte.
	sta	irqptr+1
out:
	rts
.endproc

	.segment	"LOWCODE"
;;; The weed ripens quickly.
.proc	ripen_weed
	cmp	#WEEDCHAR
	beq	ripen
	cmp	#WEEDCHAR+1
	beq	ripen
	cmp	#WEEDCHAR+2
	beq	ripen
end:	rts
ripen:
	jsr	xorshift
	and	#%10011001
	bne	end
	lda	irqptr
	sta	dest
	lda	irqptr+1
	sta	dest+1
	ldx	#0
stem:	inc	$FFFF,x
	dest=*-2
	txa
	bne	end
	clc
	adc	#40
	tax
	jmp	stem
.endproc

	.code
;;; Iterate through all the world screen-positions.
;;; The function is called with A=content of the screen cell, Y=0. The irqptr points the screen cell.
;;; Input: A/X=function to be called at each screen position.
;;; Output: -
;;; Modifies: A, X, Y
.proc	iterate_world
	sta	fptr
	stx	fptr+1
	lda	#<TOPADDR
	sta	irqptr
	lda	#>TOPADDR
	sta	irqptr+1
loop:	ldy	#0
	lda	(irqptr),y
	jsr	64738
	fptr = *-2
	inc	irqptr
	bne	skip
	inc	irqptr+1
skip:
	lda	#<ENDADDR
	cmp	irqptr
	bne	loop
	lda	#>ENDADDR
	cmp	irqptr+1
	bne	loop

	rts
.endproc

.segment "LOWCODE"
;;; Check current position for ripe weed and if the time is right make it explode.
;;; Input: A=character, irqptr
;;; Output: -
;;; Modifies: A, X, Y
.proc	way_of_the_exploding_weed
	cmp	#WEEDCHAR+3	; Is it ripe?
	beq	explode
end:	rts
explode:
	jsr	xorshift
	and	#%00010000	; They really explode!
	bne	end
	;; Clear old plant.
	lda	#EMPTYCHAR
	sta	(irqptr),y
	ldy	#40
	sta	(irqptr),y
	jsr	get_random_free_cell
	lda	#0
	jsr	draw_weed
	jsr	get_random_free_cell
	lda	#0
	jmp	draw_weed
.endproc


.segment "LOWCODE"
;;; Check current position for waterd plants and if the time is right make it proliferate.
;;; Input: A=character, irqptr
;;; Output: -
;;; Modifies: A, X, Y, tmp
.proc	proliferate_flowers

	and	#%11111100	; Mask flower bits out.
	cmp	#FLOWERCHAR
	bne	end
	lda	irqptr+1	; High byte of screen pointer.
	pha			; Save it.
	eor	#$d8 ^ >SCREEN_BASE ; Now pointing to the screen colour ram.
	sta	irqptr+1
	lda	(irqptr),y	     ; Get value from colour RAM.
	and	#$0F		     ; Use only lower nibble.
	cmp	#WATEREDFLOWERCOLOUR ; Watered flower is immune
	bne	not_watered

	jsr	xorshift	; Get a random number
	cmp	#PROBFLOWPROL	; Compare to "out-of-my-sleeve" value.
	bcc	not_watered	; Value not above threshhold, do nothing.
	sta     planttype+1	
	and     #03  ;isolate lowest 2 bits
	tax
	
	;get one out of 4 random neighbor positions
	;carry is still set from cmp	#PROBFLOWPROL
	lda	irqptr
	sbc     neighboorhood_lo,x
	sta     ptr
	lda     irqptr+1
	sbc     neighboorhood_hi,x
	sta     ptr+1

	lda     (ptr),y
	cmp	#EMPTYCHAR
	bne 	not_watered  ;this place is already taken
planttype:
	lda	#00	;value will be overwritten
	and     #03
	jsr	draw_flower
not_watered:
	pla			; Get old high-byte.
	sta	irqptr+1
end:	rts
.endproc

neighboorhood_lo: .byte <$D7,<$D9,39,41  ;northeast:-41, northwest:-39, southeast:+39, southwest:+41
neighboorhood_hi: .byte $d8 - >SCREEN_BASE - 1,$d8 - >SCREEN_BASE - 1,$d8 - >SCREEN_BASE,$d8 - >SCREEN_BASE  ;highbytes adjusted with correction $d8 - >SCREEN_BASE 

.code 

; Check for living plants, if only dead then Game Over.
;;; Input: A=character, irqptr
;;; Output: -
;;; Modifies: A, X, Y
	.proc check_plants
	.segment	"SIXTEEN"
theylive:	.res	1	; Counter of living (non-watered plants).
watered:	.res	1	; Watered flower counter.
dead:		.res	1	; Dead flower counter.
weeds:		.res	1	; Number of weeds.
	.code
	and	#%11111100	; Mask flower bits out.
	cmp	#FLOWERCHAR
	beq	plant		; Plant found.
	cmp	#WEEDCHAR
	beq	weed
	rts
weed:	inc	weeds
	bne     @skip
	dec	weeds 	;we have already 255 weeds
@skip:	rts
plant:	lda	irqptr+1	; Save old high byte.
	pha
	add	#$D8->SCREEN_BASE ; Adjust for colour ram.
	sta	irqptr+1
	lda	(irqptr),y
	and	#$f		; Colour in low nibble.
	cmp	#DEADFLOWERCOLOUR
	bne	notd		; Not a dead flower.
	inc	dead
	bne	out		; Branch always.
notd:	cmp	#WATEREDFLOWERCOLOUR
	bne	nonw
	inc	watered
	bne	out
nonw:	inc	theylive
out:	pla
	sta	irqptr+1
	rts
.endproc

;;; Update the game screen with a round robin function.
.proc	update_world
	lda	frame_flags
	eor	#%10000000
	sta	frame_flags
	dec	frame_stepper
	beq	run
	jmp	handle_monster
run:	lda	#NTHFRAME
	sta	frame_stepper
	lda	#<dry_flower
	ldx	#>dry_flower
	jsr	iterate_world	; Perform action
	;; Ripen or Exlode weed.
	bit	frame_flags
	bmi	explode
	lda	#<ripen_weed
	ldx	#>ripen_weed	; High value must be > 0!
	bne	call
explode:
	lda	#<way_of_the_exploding_weed
	ldx	#>way_of_the_exploding_weed
call:	jsr	iterate_world	; Perform action
	lda	#0
	sta	check_plants::theylive	; Plants are living.
	sta	check_plants::watered	; Plants are not thirsty.
	sta	check_plants::dead	; Plants are dead.
	sta	check_plants::weeds	; Weeds.
	lda	#<check_plants
	ldx	#>check_plants
	jsr	iterate_world
	lda	check_plants::theylive
	ora	check_plants::watered
	beq	gaov		; Game Over, no living plants.
	;; Check if too many weeds.
	lda	check_plants::theylive
	clc
	adc	#PLANTBONUS
	adc	check_plants::watered
	sbc	check_plants::weeds
	bmi	weedgaov
	;; Now lets grow them!
	lda	#<proliferate_flowers
	ldx	#>proliferate_flowers
	jsr	iterate_world

	;add some new weed, just for the fun of it
	jsr	get_random_free_cell
	lda	#0
	jsr	draw_weed

end:	rts

gaov:	lda	#0
	sta	game_running	; Game Over
	rts
weedgaov:			; Weed game over.
	lda	#139
	ldx	#1
	jsr	draw_multiple_plants
	ldx	#10
	jsr	wait_x_frames
	jmp	gaov
.endproc


