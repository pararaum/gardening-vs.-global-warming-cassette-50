; -*- mode: asm -*-
; -------------------------------------------------------------------------
; Preset values
; -------------------------------------------------------------------------
	.include "sprites.i"

;;; Set this to one in order to use the functions instead of the includes.
	COMPILE_AS_FUNCTION = 1
	DISTRIBUTOR = 0

GAMESPRCOSTUME_UPDOWN= 2 ;$80
WATERCANSPRCOSTUME   = 3 ;$c0
GAMESPRCOSTUME_RIGHT = 5 ;$140/180
RAKESPRCOSTUME       = 7 ;$1c0
DEADPLANTSPRCOSTUME  = 15 ;$3c0
FLASKSPRCOSTUME      = 30 ;$780 only works if screen clear does spare this area

GAMESPR_NO    =0
WATERCANSPR_NO=1
RAKESPR_NO    =2
FLASKSPR_NO   =3
MONSTERSPR_NO   =7

JOYPORT= 2 ;use numbers 1 or 2
WALKSPEED= 2
FIRETRIGGER= 3 ;number of frames until firebutton triggers an action
ANIMATIONSPEED=(1<<3) ;needs to be an exponent of 2
PICKUPCOOLDOWN=40

SNOWMONSTER_MASK = $1f
FIREMONSTER_MASK = $03

SNOWMONSTER_VALUE = 100
FIREMONSTER_VALUE = $80

.ifndef SCREEN_BASE
SCREEN_BASE=$400
.endif

CARRYNOTHING  = 2*GAMESPR_NO
CARRYWATERCAN = 2*WATERCANSPR_NO
CARRYRAKE     = 2*RAKESPR_NO
CARRYFLASK    = 2*FLASKSPR_NO

GAMESPRCOLOR=10
WATERCANSPRCOLOR=3
RAKESPRCOLOR=15
FLASKSPRCOLOR=HEATFLASKCOL

SPRMUCO1=13
SPRMUCO2=9

GAMESPR_IS_MUCO=1
WATERCANSPR_IS_MUCO=0
RAKESPR_IS_MUCO=1
FLASKSPR_IS_MUCO=0

MONSTERENDY=	254

LOOKRIGHT=0
LOOKLEFT=1

	.importzp	playerx
	.importzp	playery	; Y position of the player, the corresponding character line can be calculated via (playery-50)/8.
	.importzp	watercanx  ;CAVE! watercanx must be 2*(WATERCANSPR_NO-GAMESPR_NO) after playerx
	.importzp	control
	.importzp	rakex  ;CAVE! rakex must be 2*(WATERCANSPR_NO-RAKESPR_NO) after playerx
	.importzp	firecounter
	.importzp	flaskx  ;CAVE! flaskx must be 2*(WATERCANSPR_NO-FLASKSPR_NO) after playerx
	.importzp	moved
	.importzp	animcounter
	.importzp	carrying
	.importzp	pointer1 ;2 bytes
	.importzp	flips ;counter for number of sprite flips to avoid running out of time when flipping too many sprites
	.importzp	dir ;looking right:0, looking left:1
	.importzp	temp   ;reusing this address in sprite mirror function
	.importzp	char_ptr   ;will point to char in screen when fire button is pressed
	.importzp	scrn_line  ;line of the screen where fire was pressed
	.importzp	pickupcounter
	.importzp	Xbit9
	.importzp	monsterx, monster_type ; X position of monster and monster type (SNOWMONSTER_VALUE,FIREMONSTER_VALUE)

PLAYERSTARTX=  149
PLAYERSTARTY=  100
WATERCANSTARTX=300
WATERCANSTARTY=80
RAKESTARTX=    300
RAKESTARTY=    130
FLASKSTARTX=    300
FLASKSTARTY=    180

PLAYER_MIN_Y=  58
PLAYER_MAX_Y=  228
PLAYER_MIN_X=  24
PLAYER_MAX_X=  324
