;;; Everything in here can only be used during the initialisation phase.
;;; Once "main" has been called these memory areas are *partially* destroyed/reused.
;;;
;;; Be very careful when using something here. Everything is chaotic as we had to save as many bytes as possible. Damn the 4K limit.

	.import	main
	.include	"consts.i"
	.include	"vars.i"
;	.include	"sprites.i"
	.include	"presets.i"
	.include	"libt7d.i"
	.include	"irq.i"
	.include	"muzak.i"
	.include	"vars.i"
	.include	"monster.i"

	.import	__ZP_SIZE__
	.import	__ZP_START__
	.import	__BSS_RUN__, __BSS_SIZE__

	.export	setupirq
	.export init_game_world

	.segment "JUMP"
	;; This segment is positioned at $0200 and must be exactly
	;  eight bytes long so that the next memory describes a
	;  character.
	;  It can be overwritten as it is only needed during autostart.
	nop
	nop
	nop
	nop
	nop
	jmp	entry

	.segment "EXEHDR"
;;; This was moved here, in order to free some space up.
setupirq:
	lda	#<irqTOP
	sta	$314
	lda	#>irqTOP
	sta	$315
	lda	#TOPRASTER		; Set raster line.
	sta	$d012
	lda	$d011
	and	#$7f
	sta	$d011
	jsr	_disable_cia_irq
	lda	#%00000001	; Raster interrupt.
	sta	$d01a
	rts


	.segment "EXEHDR"
	;; This is positioned at $0102 called by an RTS from a stack filled with $01.
	;; This code would be overwritten by normal stack usage, therefore it is cleared in stack_clear anyway. Look in the monitor for stack shortage.
entry:	sei
	ldx	#STACKSIZE-1	; 6502 stack: first write then decrement
	txs
	lda	#BACKGROUND
	sta	$d020
	sta	$d021
	lda	#1		; white
	sta	$d022		; ECM background …01
	lda	#$a		; red
	sta	$d023		; ECM background …10
	lda	#$e		; blue
	sta	$d024		; ECM background …11
	;; Make characters with code and data invisible on the screen.
	lda	#BACKGROUND
	ldx	#0
@col:	sta	$db00,x
	dex
	bne	@col
	jmp	once


	.segment "VECTORS"
.proc	init_game_world
	lda	#1
	sta	level
	lda	#0
	sta	score
	sta	score+1
	sta	randomoff

	sta	heat_resistance
	sta	cold_resistance
	ldx	#2*40-1
	bne     init_gw2	;unconditional branch 
.endproc

.assert * < $315, error, "IRQ vector later than $314, remove code"
.assert * > $313, error, "IRQ vector earlier than $314, add NOPs"
	.word	$EA31		; IRQ routine
	.ifdef NDEBUG
	.word	64738		; BRK routine
	.word	64738		; NMI routine
	.else
	.word	broken
	.word	broken
	.endif

init_gw2:
cploop:	lda	header_line,x
	sta	__SCREEN_START__,x
	dex
	bpl	cploop
	rts
	nop
	nop
.assert * < $327, error, "CHROUT vector later than $326, remove code"
.assert * > $325, error, "CHROUT vector earlier than $326, add NOPs"
	.word	$0202		; CHROUT
	.word	$F6ED		; STOP routine

	.ifndef	NDEBUG
broken:
	inc	$d020
	jmp	broken
	.endif

.segment "LOWCODE"
	.include "move_ghislain/tables.inc"

	;here comes the routine from file screen.s
	;just because it fits here
;	.export	clear_screen
;	.export	fill_screen

;
;.proc	clear_screen
;	lda	#FREESPACE
;onlyfill:
;	ldx	#0
;@l1:
;	sta	SCREEN_BASE+2*40,x
;	sta	SCREEN_BASE+$100,x
;	sta	SCREEN_BASE+$200,x
;	sta	SCREEN_BASE+$300-$80,x
;	dex
;	bne	@l1
;	rts
;.endproc
;	fill_screen = clear_screen::onlyfill


	.data
header_line:
	.byte	$13, $03, $0F, $12, $05, $3A, $20, $30, $30, $30, $30, $30, $30, $20, $20, $20, $0C, $05, $16, $05, $0C, $3A, $20, $30, $30, $20, $20, $20, $14, $09, $0D, $05, $3A, $20, $30, $30, $30, $30, $30, $30
	.byte	$20, $20, $20, $08, $05, $01, $14, $20, $12, $05, $13, $09, $13, $14, $01, $0E, $03, $05, $20, $20, $20, $20, $03, $0F, $0C, $04, $20, $12, $05, $13, $09, $13, $14, $01, $0E, $03, $05, $20, $20, $20



;;; This code is executed once during initialisation. Warning! As this
;░▒▓	code resides in the screen memory clearing the screen will
;░▒▓	destroy it. Perform initialisations and after jumping to main
;░▒▓	do not leave anything here which needs to be used during
;░▒▓	gameplay.

	.segment "ONCE"
once:
	;; Clear the low memory.
	ldx	#<__ZP_SIZE__
	lda	#0
@l1:	sta	z:<__ZP_START__,x
	dex
	bpl	@l1
	;; Clear the '@' $40 screen code.
	ldx	#8-1
@l2:	sta	$200,x		; still A=0
	dex
	bpl	@l2
	;; Clear bss, A is still zero!
	ldx	#0
@l3:	sta	__BSS_RUN__,x
	inx
	.assert	__BSS_SIZE__ < 256, error, "BSS too big"
	cpx	#<__BSS_SIZE__
	bne	@l3

	sei	;interrupts off
	ldx keybuffer_values_end - keybuffer_values - 1
@loop:   lda keybuffer_values,x
	sta 631,x
	dex
	bpl @loop

	lda	#<spr_down0
	ldx	#>spr_down0
	ldy	#GAMESPRCOSTUME_UPDOWN
	jsr	copysprt
	lda     #0
	sta	dir ;looking right
	sta 	updating_world

	lda	#<spr_watercan
	ldx	#>spr_watercan
	ldy	#WATERCANSPRCOSTUME
	jsr	copysprt


	lda	#<spr_rake
	ldx	#>spr_rake
	ldy	#RAKESPRCOSTUME
	jsr	copysprt

	lda	#<spr_right0
	ldx	#>spr_right0
	ldy	#GAMESPRCOSTUME_RIGHT
	jsr	copysprt

	lda	#<spr_right1
	ldx	#>spr_right1
	ldy	#GAMESPRCOSTUME_RIGHT+1
	jsr	copysprt

	;; Now set up the flower colours...
	lda	#2
	sta	flowercolours
	lda	#8
	sta	flowercolours+1
	lda	#7
	sta	flowercolours+2
	lda	#10
	sta	flowercolours+3

	;; Set default monster value.
	lda	#SNOWMONSTER_VALUE
	sta	monsterval

	;; Initialisation of Sound moved here.
	jsr	setupsid
	jsr	setupvic

        ;; setup_sprite colors and costumes
	.include "move_ghislain/init_once_sprites.inc"

	jsr 	install_zp_code

	jmp	main

setupvic:
	lda	#1<<MONSTERSPR_NO ; Make the sprite double width (monster).
	sta	$d01d
	rts

keybuffer_values:
	.byte $18,$00,$38,$6c,$de,$be,$fe,$fe,$74,$38
keybuffer_values_end:


;;; Input: A/X=input data, Y=destination VIC sprite pointer
.proc	copysprt
	sta	ptr
	stx	ptr+1
	lda	#0
	sta	ptr2+1
	sty	ptr2
	ldx	#6
@l1:	asl	ptr2
	rol	ptr2+1
	dex
	bne	@l1
	ldy	#64-1
@l2:	lda	(ptr),y
	sta	(ptr2),y
	dey
	bpl	@l2
	rts
	
.endproc

sprite_block_0:  ;3 sprites going to $40 ++

spr_down0:

.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$02
.byte $aa,$00,$02,$ee,$00,$02,$aa,$00,$02,$aa,$00,$00,$a8,$00,$00,$54
.byte $00,$03,$57,$00,$0d,$55,$c0,$0c,$54,$c0,$08,$54,$c0,$00,$fc,$80
.byte $00,$44,$00,$00,$44,$00,$00,$44,$00,$03,$c4,$00,$00,$0f,$00,$8a

;spr_down1:
;
;.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$02
;.byte $aa,$00,$02,$ee,$00,$02,$aa,$00,$02,$aa,$00,$00,$a8,$00,$00,$54
;.byte $00,$03,$57,$00,$0d,$55,$c0,$0c,$54,$c0,$0c,$54,$80,$08,$fc,$00
;.byte $00,$44,$00,$00,$44,$00,$00,$44,$00,$00,$4f,$00,$03,$c0,$00,$8a

spr_watercan:

.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$03,$e0,$00,$06,$3e
.byte $00,$07,$fb,$00,$07,$f0,$00,$07,$f0,$00,$03,$e0,$00,$00,$00,$06

sprite_block_1: ;3 sprites going to $140 ++

;spr_up0:
;
;.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$03
;.byte $ff,$00,$03,$ff,$00,$03,$ff,$00,$02,$fe,$00,$00,$a8,$00,$00,$54
;.byte $00,$03,$57,$00,$0d,$55,$c0,$0c,$54,$c0,$08,$54,$c0,$00,$fc,$80
;.byte $00,$44,$00,$00,$44,$00,$00,$44,$00,$03,$c4,$00,$00,$0f,$00,$8a
;
;spr_up1:
;
;.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$03
;.byte $ff,$00,$03,$ff,$00,$03,$ff,$00,$02,$fe,$00,$00,$a8,$00,$00,$54
;.byte $00,$03,$57,$00,$0d,$55,$c0,$0c,$54,$c0,$0c,$54,$80,$08,$fc,$00
;.byte $00,$44,$00,$00,$44,$00,$00,$44,$00,$00,$4f,$00,$03,$c0,$00,$8a

spr_right0:
	.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$03
	.byte $e8,$00,$03,$ec,$00,$00,$e8,$00,$00,$a8,$00,$00,$20,$00,$00,$70
	.byte $00,$00,$70,$00,$00,$70,$00,$00,$70,$00,$00,$5e,$00,$00,$f0,$00
	.byte $00,$50,$00,$00,$54,$00,$01,$44,$00,$01,$0f,$00,$03,$c0,$00,$8a

spr_right1:
	.byte $00,$00,$00,$00,$54,$00,$00,$54,$00,$01,$55,$00,$05,$55,$40,$03
	.byte $e8,$00,$03,$ec,$00,$00,$e8,$00,$00,$a8,$00,$00,$20,$00,$00,$70
	.byte $00,$00,$7c,$00,$03,$5c,$00,$03,$5f,$80,$00,$50,$00,$00,$f0,$00
	.byte $00,$50,$00,$00,$50,$00,$00,$50,$00,$00,$50,$00,$00,$fc,$00,$8a

spr_rake:

.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$28,$00,$00,$28,$00,$00,$3a,$00,$00
.byte $30,$00,$00,$f0,$00,$00,$c0,$00,$00,$c0,$00,$00,$c0,$00,$00,$c0
.byte $00,$00,$c0,$00,$03,$c0,$00,$03,$00,$00,$03,$00,$00,$00,$00,$8c

install_zp_code:

	ldy #end_zp_set_updownspr - zp_set_updownspr - 1 ;end_zp_code - zp_code -1
@loop:	lda zp_code,y
	sta zp_set_updownspr,y
	dey
	bpl @loop
	rts


zp_code:
.org $46
zp_set_updownspr:
.include "move_ghislain/set_updownspr.inc"
end_zp_set_updownspr:
.assert end_zp_set_updownspr<=$80,error,"Code in ZP overflows $80"

	.segment	"SPR780"
spr_erlenmeyerflask:

.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $78,$00,$00,$30,$00,$00,$30,$00,$00,$48,$00,$00,$48,$00,$00,$a4
.byte $00,$00,$a4,$00,$01,$42,$00,$01,$02,$00,$00,$fc,$00,$00,$00,$0f

;rake_costume_data:
;	.byte $00,$00,$00,$00,$28,$00,$00,$20
;	.byte $00,$00,$a0,$00,$02,$8a,$00,$02
;	.byte $28,$00,$0a,$a2,$80,$0a,$8a,$00
;	.byte $0a,$28,$00,$0a,$a0,$00,$08,$00
;	.byte $00,$08,$00,$00,$08,$00,$00,$08
;	.byte $00,$00,$0a,$00,$00,$0a,$00,$00
;	.byte $0a,$00,$00,$0a,$00,$00,$0a,$00
;	.byte $00,$00,$00,$00,$00,$00,$00,$83
	
