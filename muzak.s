	.export	setupsid
	.export stop_sfx
	.export	play_bing
	.export	play_saw
	.export	play_hiss
	.export	play_watering

FREQHISS = $0255
FREQWATER = $7497

	;; This is called earlier in time and may be moved into once.
	.segment	"ONCE"

;;; SoundFX table, see http://www.oxyron.de/html/registers_sid.html

voice1:	.word	$3400		; frequency
	.word	0		; pulse width
	.byte	%00001000	; control
	.byte	$42		; AD
	.byte	$CA		; SR

voice2:	.word	$1400		; frequency
	.word	0		; pulse width
	.byte	%00001000	; control
	.byte	$22		; AD
	.byte	$69		; SR

voice3:	.word	FREQHISS	; frequency
	.word	0		; pulse width
	.byte	%00001000	; control
	.byte	$24		; AD
	.byte	$7c		; SR

.proc	setupsid
	ldx	#7
@l:
	lda	voice1,x
	sta	$D400,x
	lda	voice2,x
	sta	$D407,x
	lda	voice3,x
	sta	$D40E,x
	dex
	bpl	@l
	lda	#$0f
	sta	$d418		; Sound must be LOUD!
	rts
.endproc

	.code
.proc	play_bing
	lda	#$11
	sta	$d404		; Control
	rts
.endproc

.proc	play_saw
	lda	#$21
	sta	$d40b		; Control
	rts
.endproc

.proc	play_hiss
	lda	#<FREQHISS
	sta	$d40e
	lda	#>FREQHISS
	sta	$d40f
	lda	#$81
	sta	$d412		; Control
	rts
.endproc

.proc	play_watering
	lda	#<FREQWATER
	sta	$d40e
	lda	#>FREQWATER
	sta	$d40f
	lda	#$81
	sta	$d412		; Control
	rts
.endproc

stop_sfx:
	lda	#$10
	sta	$d404
	lda	#$20
	sta	$d40b
	lda	#$80
	sta	$d412
	rts
