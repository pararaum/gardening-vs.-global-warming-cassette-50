	.include	"consts.i"
	.include	"presets.i"
	.include	"prng.xorshift.i"
	.include	"vars.i"

	.exportzp monstercounter
	.exportzp monstercolumn
	.exportzp monsterkillptr
	.exportzp monsterval
	.export	create_monster

	.zeropage
monstercounter:
	.res	1
	.zeropage
monstercolumn:
	.byte	0
monsterkillptr:
	.res	2
	.zeropage
monsterval:
	.res	1		; Set via destructibles.
;;; 	.byte	SNOWMONSTER_VALUE

	.code
; -------------------------------------------------------------------------
; Code to invoke snow/fire monster
; pass the X position in register A
; and the monster type (SNOWMONSTER_VALUE,FIREMONSTER_VALUE) in register Y
; -------------------------------------------------------------------------

create_monster:
	jsr	xorshift
	and	#$1f		; 0..31
	pha			; Push on stack
	clc
	adc	#80		; Skip first two lines.
	sta	monsterkillptr		; Store the start column to kill plants in.
	lda	#$4
	sta	monsterkillptr+1	; High byte of pointer
	pla			; Get position.
	asl			; *8 but position is multiplied by two at monster movement routine.
	asl
				; Therefore only times four!
	clc			; move to the right of the border
	adc	#23/2
	sta	monsterx
	lda	#1		; White *and* start movement.
	sta	$D027+MONSTERSPR_NO
	sta	$D001+2*MONSTERSPR_NO

	; plant killing is done in the interrupt

end:	rts

.export monsters_kill_plants
.proc monsters_kill_plants

.include "move_ghislain/monsters_kill_plants.inc"	

.endproc