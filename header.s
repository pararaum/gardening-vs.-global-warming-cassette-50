	.include	"bcdtools.i"
	.include	"consts.i"
	.include	"vars.i"
	.export	display_score
	.export	display_time
	.export	display_level

;;; Needed for the conversion of values in the header.
	.zeropage
workarea:
	.res	2
output:
	.res	3

	.code

;;; Simple conversion function, uses workarea and output.
;;; Input: A/X = 16-Bit value to convert
;;; Output: output
;;; Modifies: A, X, Y, ptr
convert:
	sta	ptr
	stx	ptr+1
	ConvertToBCD	ptr, workarea, output, 2, 3
	rts

;;; Input: x = starting position to write output to
;;; Modifies: A, X
output2screen:
	.repeat 3,i
	lda	output+i
	pha
	and	#$0f
	ora	#$30
	sta	SCREEN_BASE-2*i,X
	pla
	lsr
	lsr
	lsr
	lsr
	ora	#$30
	sta	SCREEN_BASE-1-2*i,X
	.endrepeat
	rts

.proc	display_score
	lda	score
	ldx	score+1
	jsr	convert
	ldx	#12
	jmp	output2screen
.endproc

.proc	display_time
	lda	time
	ldx	time+1
	jsr	convert
	ldx	#40-1
	jmp	output2screen
.endproc

.proc	display_level
	ConvertToBCD	level, workarea, output, 1, 1
	lda	output
	and	#$0f
	ora	#$30
	sta	SCREEN_BASE+24
	lda	output
	lsr
	lsr
	lsr
	lsr
	ora	#$30
	sta	SCREEN_BASE+23
	;; And... squeeze in the display_resistance
	lda	heat_resistance
	lsr
	tax
hl:	lda	HEATRESISTANCEPOS,x
	orA	#%10000000
	sta	HEATRESISTANCEPOS,x
	dex
	bpl	hl
	lda	cold_resistance
	lsr
	tax
cl:	lda	COLDRESISTANCEPOS,x
	orA	#%11000000
	sta	COLDRESISTANCEPOS,x
	dex
	bpl	cl
	rts
.endproc
