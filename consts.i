;;; -*- mode: asm -*-

;;; Linker constants...
	.import	__SCREEN_START__	; This is also the screen base.
	.import	__SCREEN_SIZE__		; This is the number of usable screen bytes. After that: Sprite!

;;; This file contains important constants.
STACKSIZE = $40
BACKGROUND = 5
FOREGROUND = 9
TOPRASTER = 50+2*8
BOTRASTER = 250

;;; 50 fps, 37s = (* 50 37)1850, nearest prime number is 1847. We use the prime number to make the starting time look more mysterious.
TIMEPERLEVEL = 1847

;;; Probability of Flower Proliferation. A random value (8-Bit) is compared to this number.
PROBFLOWPROL = $C6   ;this should be a multiple of 4 to ensure fair growth in all dirs

COLDRESISTANCEPOS = __SCREEN_START__+40*1+22
HEATRESISTANCEPOS = __SCREEN_START__+40*1+3

;;; Colours of Flowers
DEADFLOWERCOLOUR = 11
DRIEDFLOWERCOLOUR = 9
WATEREDFLOWERCOLOUR = 6

;;; Colours of flasks
	HEATFLASKCOL = 2		    ; red
	COLDFLASKCOL = 6		    ; blue

;;; Screen is put into the "ONCE" segment.
	SCREEN_BASE := __SCREEN_START__

	EMPTYCHAR = $40
	WALLCHAR = $41

;;; Beginning of the flower characters.
	FLOWERSTEM = $48
	FLOWERCHAR = $48+4

;;; Beginning of the weed characters.
	WEEDSTEM = $50
	WEEDCHAR = $50+4


WEEDPOINTS = 1

;;; To avoid immediate loss at the beginning

PLANTBONUS = 10


;;; List of four flower colours.
	.importzp	flowercolours
