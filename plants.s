	.include	"vars.i"
	.include	"consts.i"
	.include	"prng.xorshift.i"

	.export	draw_flower
	.export	draw_weed
	.export get_random_xy_type
	.export	draw_multiple_plants
	.export get_random_free_cell

;;; Get random x and y positions and a plant type.
;;; Input: -
;;; Output: A=0..3, X=x-pos, Y=y-pos
;;; Modifies: A, X, Y
.proc	get_random_xy_type
	jsr	xorshift	; Get next random number
	pha			; Store for later use
	and	#$f		; Only 16 positions
	clc
	adc	#(25-16-2)/2	; Center
	tay
	txa			; Get high byte
	and	#$1f		; 32 positions.
	clc
	adc	#(40-32)/2	; Center
	tax
	pla			; Get random value back
	rol			; Now we use the top left two bits
	rol
	rol
	and	#%00000011	; mask out remaining garbage
	rts
.endproc

;;; Get a random free cell (upper and lower part).
;;; Input: -
;;; Output: A=0..3, ptr=points to the free cell
;;; Modifies: A/X/Y
.proc	get_random_free_cell
;;; The are usable is 0x400 to 0x780 (sprite begins there), so we have to start within 0x740. 0x340 (offset) times four is 0xd00. So we will run the loop until we find something suitable.
notsuitable:
	jsr	get_random_xy_type
	pha			; Store the type.
	jsr	y_times_40	; Set pointer ptr to offset.
	txa			; X-position into A.
	clc
	adc	ptr		; Add to low.
	sta	ptr
	tax			;store A for later
	lda	#>SCREEN_BASE
	adc	ptr+1		; Add high byte and carry from above.
	sta	ptr+1
	txa			;recover value
	adc 	#40		;calculate address with offset 40
	sta 	ptr2
	lda	ptr+1
	adc	#00
	sta	ptr2+1
	
	;; TODO: Check if it is empty!
	ldy	#0
find_top:
	lda	(ptr),y		; Get contents of screen.
	beq	no_find_end_of_screen_panic  ;we did not find a single empty spot and ran into the sprite below the screen
	cmp	#EMPTYCHAR
	bne	next_char
	lda	(ptr2),y	; check line below
	cmp	#EMPTYCHAR
	beq	found_both_empty
next_char:
	iny
	bne	find_top

no_find_end_of_screen_panic:
	lda #<($770-$28)	;use the spot in lower left corner
	sta ptr
	lda #>($770-$28)
	sta ptr+1
	pla
	rts

found_both_empty:
	tya
	clc
	adc	ptr		; And add to the pointer.
	sta	ptr
	bcc	no_hi_inc
	inc	ptr+1

no_hi_inc:	
	pla			; Restore the type.
	rts

.endproc

;;; Multiply Y register times 40 and store the result in ptr.
;;; Input: Y = line to use
;;; Output: ptr = offset from the screen base.
;;; Modifies: A
.proc y_times_40
	lda	#0
	sta	ptr+1		; Clear high of pointer
	tya			; A = y-pos
	asl			; Multiply by 8
	asl
	asl
	pha
	sta	ptr		; Low byte of pointer.
	asl	ptr		;*16
	rol	ptr+1
	asl	ptr		;*32
	rol	ptr+1
	pla			; y-pos*8
	clc
	adc	ptr
	sta	ptr
	bcc	@noinc
	inc	ptr+1
@noinc:
	rts
.endproc


;;; Draw a flower at the given cell.
;;; Input: A = flower type, ptr=pointer to cell
;;; Output: -
;;; Modifies: A, X, Y, ptr2
.proc	draw_flower
	.code
	tax			; Save the flower type. ①
	clc
	adc	#FLOWERSTEM
	jsr	draw_plant
	lda	flowercolours,x
	ldy	#0
	sta	(ptr2),y	; Set colour.
	rts
.endproc

;;; Draw a plant at (ptr) and colour light green at (ptr2).
;;; Input: A=plant char, ptr, ptr2
;;; Output: Y=40
;;; Modifies: A, Y
draw_plant:
	ldy	#40		; Go to stem position.
	sta	(ptr),y		; Write character.
	clc
	adc	#4		; Difference between stem and head character-code.
	ldy	#0
	sta	(ptr),y		; Write head character.
	lda	ptr		; Keep the pointer save.
	sta	ptr2
	lda	ptr+1		; High byte.
	clc
	adc	#$d8->SCREEN_BASE		; Colour RAM
	sta	ptr2+1
	lda	#13		; Light green.
	sta	(ptr2),y
	ldy	#40
	sta	(ptr2),y
	rts

;;; Input: A = weed type, ptr=pointer to cell
;;; Output: -
;;; Modifies: A, Y, ptr2
.proc	draw_weed
	.code
	lda	#WEEDSTEM
	jmp	draw_plant
.endproc


.proc	draw_multiple_plants
	.segment	"SIXTEEN"
	.define	plantf draw_flower, draw_weed
funcshi:	.hibytes	plantf
funcslo:	.lobytes	plantf
	.segment	"SIXTEEN"
counter:	.res	1
	.code
	sta	counter
	lda	funcslo,x
	sta	fptr
	lda	funcshi,x
	sta	fptr+1
loop:	jsr	get_random_free_cell
	jsr	64738
	fptr=*-2
	dec	counter
	bne	loop
	rts
.endproc
