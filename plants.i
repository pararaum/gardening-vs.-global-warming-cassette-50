; -*- mode: asm -*-
	.import	draw_flower
	.import draw_weed
	.import get_random_xy_type
	.import	get_random_free_cell

;;; Input: A=how many, x=type 0 (plant) or 1 (weed)
;;; Output: -
;;; Modifies: A, X, Y
	.import draw_multiple_plants
