; -*- mode: asm -*-

;;; Handle monster activity, called once per frame.
;;; Input: -
;;; Output: -
;;; Modifies: A, X, Y
;;; 	.import	handle_monster

;;; Creates the sprite and kills the flowers.
	.import	create_monster

;;; Count number of frames since last monster appeared.
	.importzp	monstercounter

;;; In which screen column the monster starts.
	.importzp	monstercolumn
;;; We need a pointer in the zeropage for indirect addressing. (We can not reuse the other ones as long running threads may interfere)
	.importzp	monsterkillptr
;;; Either SNOWMONSTER_VALUE or FIREMONSTER_VALUE.
	.importzp	monsterval
