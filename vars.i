; -*- mode:asm -*-

	.importzp	heat_resistance
	.importzp	cold_resistance
	.importzp	level
	.importzp	score
	.importzp	time
;;; This byte is zero if the game is not running. It will be set to a non-zero value when the game is running and every frame some action must be done.
;;; BIT 7: 1=game running
;;;	6: 1=level finished
;;;	?: ?
	.importzp	game_running

;;; Offset to the current PRNG seed. (1 Byte)
	.importzp	randomoff

;;; Temporary pointer
	.importzp	ptr
	.importzp	ptr2

;;; Temporary variable
	.importzp	tmp
	.importzp	tmp2

;;; Increase the current score value.
;;; Input: A=value to add to score
;;; Output: -
;;; Modifies: A
	.import	increase_score
;;; updating__world flag - 0 means no update, 1 means update in progress
	.importzp	updating_world