;;; -*- mode: asm -*-

;;; Calls all the necessary functions to display the header.
.macro	DisplayHeader
	jsr	display_score
	jsr	display_time
	jsr	display_level
.endmacro

	.import	display_score
	.import	display_time
	.import display_level
