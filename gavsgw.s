	.include	"consts.i"
	.include	"vars.i"
	.include	"libt7d.i"
	.include	"kernal.i"
	.include	"prng.xorshift.i"
	.include	"muzak.i"
	.include	"ghislain.i"
	.include	"sprites.i"
	.include	"header.i"
	.include	"world.i"
	.include	"plants.i"
	.include	"screen.i"
	.include	"presets.i"
	.include	"destructibles.i"

	.macpack	cbm

	;; This function has been moved into the destructibles. Be careful!
	.import	init_game_world, wait_x_frames

	.export	main		; Export main, there the game begins.
	.export irqTOP
	.export irqBOT

	.segment	"LOADADDR"
	.word	$100
	
	.code
irqTOP:
	asl	$d019
	lda	#%00011011	; No ECM, pure Text mode
	sta	$d011
	bit	game_running
	bpl	@skip
	lda	#%00010000	; Screen@$0400, Chargen@$0000
	sta	$d018
	;; Decrement time.
	lda	time
	bne	@skip2
	dec	time+1
	@skip2:
	dec	time
	;; End of time reached?
	lda	time
	ora	time+1		; Is it zero?
	bne	@skip3
	lda	#%01000000	; Bit 6: end of level
	sta	game_running
	@skip3:
	;; Update IRQ parameters for Poor Man's Threading.
	lda	#<irqBOT
	sta	$314
	lda	#>irqBOT
	sta	$315
	lda	#BOTRASTER
	sta	$d012
	DisplayHeader
	bit	updating_world
	bne	@skip_update_world
	inc	updating_world	;set updating_world to 1
	;; Poor Man's Threading...
	cli
	jsr	update_world
	dec	updating_world ;set updating_world to 0, allow further updating
@skip_update_world:

@skip:
	.ifdef	DEBUG
	lda	$d012
	sta	$d020
	.endif
	jmp	EXITIRQ

irqBOT:
	asl	$d019
	lda	#%00010100	; Screen@$0400, Chargen@$1000
	sta	$d018
	.ifdef	DEBUG
	lda	$d012
	sta	$d020
	.endif
	lda	#<irqTOP
	sta	$314
	lda	#>irqTOP
	sta	$315
	lda	#%01011011	; ECM Text mode
	sta	$d011
	lda	#TOPRASTER
	sta	$d012
	jsr	stop_sfx
	bit	game_running
	bpl	@skip
	jsr	sprite_movement
	;; Now change the colour of the Erlenmeyer flask.
	lda	#CARRYFLASK
	cmp	carrying
	beq	@skip
	lda	time
	bne	@skip
	lda	$d027+FLASKSPR_NO ; Get old colour.
	eor	#HEATFLASKCOL^COLDFLASKCOL
	sta	$d027+FLASKSPR_NO
@skip:
	jmp	EXITIRQ

	; copying from back to front to save three bytes
	.proc	press_fire
	.segment	"SIXTEEN"
text:	scrcode	"FIRE!"
endtext:
	.code
	ldx	#endtext-text-1
@l:	lda	text,x
	sta	SCREEN_BASE+12*40+18,x
	dex
	bpl	@l

	ldx	#50		;delay 1 second
	jsr	wait_x_frames

@out:	lda	#%00010000
@l2:
	and	$dc00
	bne	@l2
	rts
	.endproc

.proc	init_level
	PRNGDATA = $A770	; Make sure there are *no* two consecutive zero bytes in a page otherwise the PRNG will stop working. We initialise the PRNG with two bytes (increasing the offset) so that the levels are somewhat consistent.
	.code
	;; Init PRNG.
	ldy	randomoff
	lda	PRNGDATA,y
	tax
	iny
	lda	PRNGDATA,y
	iny
	sty	randomoff
	jsr	xorshift_seed
	;; Generate playfield. Draw flowers first as only the weeds check for occupied cells.
	lda	level		; Get level and divide by 8.
	lsr
	lsr
	lsr
	clc
	adc	#4		; Add four flowers and put into A.
	ldx	#0		; Plants
	jsr	draw_multiple_plants
	lda	level		; Get level and divide by 4.
	lsr
	lsr
	clc
	adc	#3		; Add three weeds and put into A.
	ldx	#1		; Weeds
	jsr	draw_multiple_plants
	jmp	init_time_for_level
.endproc

;;; Add new plants into at each new level. Currently this adds another flower.
init_level_new_plants:
	lda	#1		; Add a single plant.
	tax
	jmp	draw_multiple_plants


;;; Set the level time to the default value
;;; Input: -
;;; Output: -
;;; Modifies: A
.proc	init_time_for_level
	lda	#<TIMEPERLEVEL
	sta	time
	lda	#>TIMEPERLEVEL
	sta	time+1
	rts
.endproc

	.segment	"LOWCODE"
titletext:	scrcode	"GARDENING VS. GLOBAL WARMING"
titletext_end:
titletext_length = titletext_end-titletext
	.code
main:	jsr	setupirq

	cli			; Now allow interrupts.
	jsr	init_game_world
game_loop:
	ldx	#titletext_length-1
@l1:
	lda	titletext,x
	sta	SCREEN_BASE+40*1+(40-titletext_length)/2,x
	dex
	bpl	@l1
	lda	#$68
	ldx	#(40-titletext_length)/2-1
@l2:	sta	SCREEN_BASE+40*1,x
	sta	SCREEN_BASE+40*2-(40-titletext_length)/2,x
	dex
	bpl	@l2
	lda	#' '
	jsr	fill_screen
	jsr	clear_colourram
	jsr	press_fire
	jsr	init_game_world
	jsr	set_sprite_starting_positions
	jsr	clear_screen
	jsr	init_level
@level_loop:
	jsr	play_saw
	jsr	init_time_for_level
	lda	#%10000000
	sta	game_running	; Start the game
	;; From now on the random number generator belongs to the interrupts. There is no provision for concurrent use. Warning! If both the game code and the irq code use the PRNG at the same time it will probably corrupt the PRNG state!
@eog:	bit	game_running
	bmi	@eog		; End Of Game
	;; Check if levelled up...
	bvs	@next_level
	;; Now the PRNG belongs to the game again and may be used in the game loop.
	jmp	game_loop
@next_level:
	inc	level
	lda	time+1		; Divide time by 256.
	clc
	adc	score
	sta	score
	jsr	init_level_new_plants
	bcc	@level_loop
	inc	score+1
	bne	@level_loop
