;;; Screen and colour RAM funtions.
	.include	"consts.i"

	.export clear_colourram
	.export fill_colourram
	.export	clear_screen
	.export	fill_screen

;;; Moving the code down to the SIXTEEN segment in order to free up some memory in the code segment.
	.segment	"SIXTEEN"
.proc	clear_colourram
	lda	#FOREGROUND
fill:	ldx	#0
@l:
	sta	$d800,x
	sta	$d900,x
	sta	$da00,x
	sta	$db00-$80,x
	dex
	bne	@l
	rts
	.endproc
	fill_colourram = clear_colourram::fill

; this was moved to vectors segment, to be sure about the order there
; it is placed in source file desctructibles
.proc	clear_screen
	lda	#EMPTYCHAR
onlyfill:
	ldx	#0
@l1:
	sta	SCREEN_BASE+2*40,x
	sta	SCREEN_BASE+$100,x
	sta	SCREEN_BASE+$200,x
	sta	SCREEN_BASE+$300-$80,x
	dex
	bne	@l1
	rts
.endproc
	fill_screen = clear_screen::onlyfill
